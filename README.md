* Première étape : l'iso de guixsd 

1. La première étape essentielle est de récupérer le fichier iso du système. Pour cela, il faut de rendre à https://www.gnu.org/software/guix/download/
puis télécharger l'iso adéquate (en général x86_64).
2. Ensuite on décompresse l'archive (.xz) :
   =xz -d guix-system-install-1.0.1.system.iso.xz=
3. Insertion d'une clé usb faisant au moins 1G (*Attention* le contenu de la clé va être supprimé) puis écriture de l'iso sur la clé (le X désigne : on peut le connaitre avec lsblk)
   =dd if=guix-system-install-1.0.1.system.iso of=/dev/sdX && sync=

(REM : sdX = sdc par exemple et non sdX = sdc1)


* Deuxième étape : l'installation 
  
** L'installation manuelle  (ctrl+alt+f3)

1. Régler la configuration de votre clavier selon ce que vous utilisez :
   =loadkeys fr=
   
2. Reseau

  =ifconfig -a=

on repere eno "quelque chose" 

  =ifconfig eno1 up=
  =dhclient -v eno1=

on verifie par 

  =ping gnu.org= 

3. partionnement du disque

=lsblk= 
permet de voir les disques 

=cfdisk=

> faire une partition de 512M en type UFI
> faire une partition du reste en linux file system


=parted /dev/sda set 1 esp on=
=mkfs .fat -F32 /dev/sda1=


!!! pour ceux qui veulent crypter (OBLIGATOIRE SUR PORTABLE) !!!!!
=cryptsetup open --type plain -d /dev/urandom/ /dev/sda2 to_be_wiped=
=lsblk=
=dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress=
=cryptsetup close to_be_wiped=
=cryptsetup -v --type luks1 --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 4000 --use-urandom --verify-passphrase luksFormat /dev/sda2=
=cryptsetup open /dev/sda2 cryptroot=
=mkfs.btrfs -L cryptroot /dev/mapper/cryptroot=
=mount /dev/mapper/cryptroot /mnt=
!!!!!!!!!! FIN DE LA PARTIE cryptage (OBLIGATOIRE SUR PORTABLE) !!!!!



!!!!!! pour ceux qui ne veulent pas crypter (Fixe par exemple) !!!!!
> on saute la phase d'encryptage pour les fixes
=mkfs.btrfs -L root /dev/sda2=
> on monte la partition :
=mount /dev/sda2 /mnt=
!!!!!!!!!! FIN DE LA PARTIE pour ceux qui ne veulent pas crypter  (Fixe par exemple) !!!!!


=cd /mnt=

=btrfs subvolume create @=
=btrfs subvolume create @snapshots=  (c'est pour les sauvegardes)
=btrfs subvolume create @home=
=btrfs subvolume create @gnu=
=btrfs subvolume create @var=   (on peut toujours utiliser d'autres sous-volumes mais là c'est déjà pas mal)


=cd ..=
=umount /mnt=


!!!!!!!!! POUR ceux qui ont fait le cryptage !!!!!!!!!!!!!
=mount -o compress=lzo,subvol=@ /dev/mapper/cryptroot /mnt=
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!! POUR ceux qui n'ont PAS fait le cryptage !!!!!!!!!!!!!
=mount -o compress=lzo,subvol=@ /dev/sda2 /mnt=
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


On crée les points de montages :

=mkidr /mnt/.snapshots=
=mkdir /mnt/home=
=mkdir /mnt/gnu=
=mkdir /mnt/var=

Puis on monte les sous-volumes :

!!!!!!!!! POUR ceux qui ont fait le cryptage !!!!!!!!!!!!!
=mount -o compress=lzo,subvol=@snapshots /dev/mapper/cryptroot /mnt/.snapshots=
=mount -o compress=lzo,subvol=@home /dev/mapper/cryptroot /mnt/home=
=mount -o compress=lzo,subvol=@gnu /dev/mapper/cryptroot /mnt/gnu=
=mount -o compress=lzo,subvol=@var /dev/mapper/cryptroot /mnt/var=
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!! POUR ceux qui n'ont PAS fait le cryptage !!!!!!!!!!!!!
=mount -o compress=lzo,subvol=@snapshots /dev/sda2 /mnt/.snapshots=
=mount -o compress=lzo,subvol=@home /dev/sda2 /mnt/home=
=mount -o compress=lzo,subvol=@gnu /dev/sda2 /mnt/gnu=
=mount -o compress=lzo,subvol=@var /devsda2 /mnt/var=
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



On crée et on monte la partition de boot :
=mkdir -p /mnt/boot/efi=
=mount /dev/sda1 /mnt/boot/efi=
On créer la swapfile :
=dd if=/dev/zero of=/mnt/swapfile bs=1MiB count=10240=
=chmod 600 /mnt/swapfile=
=mkswap /mnt/swapfile=


** La mise en place du système :


=herd start cow-store /mnt=
=mkdir /mnt/etc=

Maintenant, on va monter une clé usb contenant le fichier laptop.scm (pour les laptop) ou desktop.scm (pour les fixes) qui va bien. Je pars du principe que la clé est nommée /dev/sdc1. Un petit =ls /dev | grep sd= 
avant et après avoir mis la clé doit permettre de s'en assurer.

=mkdir cle=
=mount /dev/sdc1 ./cle=

La clé est montée à la racine du système live dans le répertoire cle. On peut copier le fichier :
=cp ./cle/config.scm /mnt/etc/.=

Et là, c'est parti :
=guix system init /mnt/etc/config.scm /mnt=


** ensuite :

=reboot=
















